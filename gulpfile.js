//  compile

const gulp = require('gulp')
const mjml = require('gulp-mjml')
const htmlmin = require('gulp-htmlmin');

gulp.task('default', function () {
    return gulp.src('mjml/*.mjml')
        .pipe(mjml())
        .pipe(gulp.dest('html'))
});

gulp.task('mjml', function(){
    return gulp.src('mjml/*.mjml')
        .pipe(mjml())
        .pipe(gulp.dest('html'))
});

gulp.task('minify', () => {
    return gulp.src('html/*.html')
        .pipe(htmlmin({ collapseWhitespace: true }))
        .pipe(gulp.dest('html/min'));
});

gulp.task('watch',function() {
	gulp.watch('mjml/*.mjml', gulp.series('mjml'))
});
